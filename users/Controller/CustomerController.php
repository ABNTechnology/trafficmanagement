<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use \TrafficManagement\users\DAO\TrafficManagementDAO;
use \TrafficManagement\users\Model\CustomerModel;
use \TrafficManagement\users\Utils\Utils;

 final class CustomerController{
   function __construct() {
 }

 public function Listing(){

    require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/Model/CustomerModel.php');
    $customerModel=new CustomerModel();
    
    $customerModeldata=$customerModel->Customerlist();
    require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/views/CustomerViewNew.php');
    
    $customerView=new CustomerViewNew($customerModeldata);
    $customerView->OutPut();
 }

 public function CustomerDetail(){
    $id=$_GET['id'];
    $arrArgument=array('id'=>$id);
    $arrValue=loadModel('CustomerModel','ShowCustomerDetails',$arrArgument);
    loadView('CustomerView.php',$arrValue);
 }
}
?>