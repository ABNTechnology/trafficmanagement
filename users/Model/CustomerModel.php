<?php

namespace TrafficManagement\users\Model;

use \TrafficManagement\users\Controller\CustomerController;
use \TrafficManagement\users\View\CustomerViewNew;
use \TrafficManagement\users\Utils\Utils;

/**
 * Model class representing one Customer item.
 */
final class CustomerModel {

    // priority
    const PRIORITY_HIGH = 1;
    const PRIORITY_MEDIUM = 2;
    const PRIORITY_LOW = 3;
    // status
    const STATUS_PENDING = "PENDING";
    const STATUS_DONE = "DONE";
    const STATUS_VOIDED = "VOIDED";

    /** @var int */
    private $id;
    /** @var string */
    private $priority;
    /** @var string */
    private $title;
    /** @var string */
    private $fName;
    /** @var string */
    private $lName;
    /** @var DateTime */
    private $createdOn;
    /** @var DateTime */
    private $lastModifiedOn;
    /** @var string one of PENDING/COMPLETED/VOIDED */
    private $status;
    /** @var boolean */
    private $deleted;


    /**
     * Create new {@link Todo} with default properties set.
     */
    public function __construct() {
        
        $this->setStatus(self::STATUS_PENDING);
        $this->setDeleted(false);
    }

    public static function allStatuses() {
        return [
            self::STATUS_PENDING,
            self::STATUS_DONE,
            self::STATUS_VOIDED,
        ];
    }

    public static function allPriorities() {
        return [
            self::PRIORITY_HIGH,
            self::PRIORITY_MEDIUM,
            self::PRIORITY_LOW,
        ];
    }

    //~ Getters & setters

    /**
     * @return int <i>null</i> if not persistent
     */
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        if ($this->id !== null
                && $this->id != $id) {
            throw new Exception('Cannot change identifier to ' . $id . ', already set to ' . $this->id);
        }
        if ($id === null) {
            $this->id = null;
        } else {
            $this->id = (int) $id;
        }
    }

    /**
     * @return int one of 1/2/3
     */
    public function getPriority() {
        return $this->priority;
    }

    public function setPriority($priority) {
        TodoValidator::validatePriority($priority);
        $this->priority = (int) $priority;
    }

    /**
     * @return DateTime
     */
    public function getCreatedOn() {
        return $this->createdOn;
    }

    public function setCreatedOn(DateTime $createdOn) {
        $this->createdOn = $createdOn;
    }

    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    public function setTitle($title) {
        $this->title = $title;
    }
    
    /**
     * @return string
     */
    public function getFName() {
        return $this->fName;
    }

    public function setFName($fName) {
        $this->fName = $fName;
    }
    
    /**
     * @return string
     */
    public function getLName() {
        return $this->lName;
    }

    public function setLName($lName) {
        $this->lName = $lName;
    }
    /**
     * @return DateTime
     */
    public function getLastModifiedOn() {
        return $this->lastModifiedOn;
    }

    public function setLastModifiedOn(DateTime $lastModifiedOn) {
        $this->lastModifiedOn = $lastModifiedOn;
    }

    
    /**
     * @return string one of PENDING/DONE/VOIDED
     */
    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        
        $this->status = $status;
    }

    /**
     * @return boolean
     */
    public function getDeleted() {
        return $this->deleted;
    }

    public function setDeleted($deleted) {
        $this->deleted = (bool) $deleted;
    }

    public function Customerlist()
    {
        require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/DAO/TrafficManagementDAO.php');
        $customerDAO=new \TrafficManagementDAO();
        
        return $customerDAO->find();
    }
    public function InsertCustomer(CustomerModel $customerModel)
    {
        require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/DAO/TrafficManagementDAO.php');
        $customerDAO=new \TrafficManagementDAO();
                
        return $customerDAO->save($customerModel);
    }
}
