$(function() {
    $("#navbarCollapse > ul > li").addClass("nav-item");
    $("#navbarCollapse > ul > li > a").addClass("nav-link");
    $("#navbarCollapse > ul > li > a .fa").remove();
    $("#navbarCollapse .dropdown-menu a").addClass("dropdown-item");
    $("#navbarCollapse > ul > li:last-child > .dropdown-menu").addClass("dropdown-menu-right");

    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
    	//alert();
	  if (!$(this).next().hasClass('show')) {
	    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
	  }
	  var $subMenu = $(this).next(".dropdown-menu");
	  $subMenu.toggleClass('show');


	  $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
	    $('.dropdown-submenu .show').removeClass("show");
	  });


	  return false;
	});
});