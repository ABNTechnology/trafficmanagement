<?php
/*
UserSpice 4
An Open Source PHP User Management System
by the UserSpice Team at http://UserSpice.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
?>
<?php require_once 'init.php'; ?>
<?php require_once $abs_us_root.$us_url_root.'users/includes/header.php'; ?>
<?php require_once $abs_us_root.$us_url_root.'users/includes/navigation.php'; ?>
<?php if (!securePage($_SERVER['PHP_SELF'])){die();} ?>
<?php
$dropdown_results = $db->query("SELECT * FROM Locations WHERE IsActive=1");
$dropdowns = $dropdown_results->results();

$dropdown_Sensor_result = $db->query("SELECT * FROM sensortype");
$dropdowns_Sensor = $dropdown_Sensor_result->results();

$errors = [];
$successes = [];

$pages=[];

//Fetch information on all pages
function fetchAllSensors() {
		$db = DB::getInstance();
		$query = $db->query("SELECT sensorid, sensorname, sensortype, sensorlandmark , locationtype FROM sensors ORDER BY sensorid DESC");
		$pages = $query->results();
		//return $pages;

		if (isset($row)){
			return ($row);
		}else{
			return $pages;
		}
}


$dbpages = fetchAllSensors(); //Retrieve list of pages in pages table

$count = 0;
$dbcount = count($dbpages);


?>
<div id="page-wrapper">

	<div class="container">
		<div class="space-top space-bottom">
		
		<!-- Page Heading -->
		<div class="row">
			<div class="col-12">

				<h1 class="h3 mb-3 font-weight-mormal">Manage Sensor Information</h1>

				<!-- Content goes here -->

				<hr>
				<table id="paginate" class='table table-hover table-list-search'>
					<thead>
						<th>Sensor Id</th><th>Sensor Name</th><th>Sensor Type</th><th>Location Type</th><th>Landmark</th>
					</thead>

					<tbody>


						<?php
						//Display list of pages
						$count=0;
						foreach ($dbpages as $page){
							?>
							<tr><td><?=$dbpages[$count]->sensorid?></td>
								<td><a class="nounderline" href ='SensorManagement_edit.php?sensorid=<?=$dbpages[$count]->sensorid?>'><?=$dbpages[$count]->sensorname?></a></td>
								<td><a class="nounderline" href ='SensorManagement_edit.php?sensorid=<?=$dbpages[$count]->sensorid?>'>
                                <?php
				foreach ($dropdowns_Sensor as $dropdown_Sensor) {
				?>
					 <?php if ($dbpages[$count]->sensortype == $dropdown_Sensor->SensorTypeID) echo $dropdown_Sensor->SensorTypeValue?>  
				<?php
				}
				?></a></td>
								<td><a class="nounderline" href ='SensorManagement_edit.php?sensorid=<?=$dbpages[$count]->sensorid?>'>
                                <?php
				foreach ($dropdowns as $dropdown) {
				?>
					 <?php if ($dbpages[$count]->locationtype == $dropdown->LocationID) echo $dropdown->LocationName?> 
				<?php
				}
				?></a></td>
                                                                <td><a class="nounderline" href ='SensorManagement_edit.php?sensorid=<?=$dbpages[$count]->sensorid?>'><?=$dbpages[$count]->sensorlandmark?></a></td>
								</tr>
								<?php
								$count++;
							}?>
						</tbody>
					</table>



				</div>
				<!-- /.row -->
			</div>
			</div>
		</div>
	</div>


	<!-- Content Ends Here -->
	<!-- footers -->
	<?php require_once $abs_us_root.$us_url_root.'users/includes/page_footer.php'; // the final html footer copyright row + the external js calls ?>

	<!-- Place any per-page javascript here -->

	<script>
	$(document).ready(function() {
	    $('#paginate').DataTable({"pageLength": 25,"aLengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]], "aaSorting": []});
	} );
	</script>
	<script src="js/pagination/jquery.dataTables.js" type="text/javascript"></script>
	<script src="js/pagination/dataTables.js" type="text/javascript"></script>

	<?php require_once $abs_us_root.$us_url_root.'users/includes/html_footer.php'; // currently just the closing /body and /html ?>
