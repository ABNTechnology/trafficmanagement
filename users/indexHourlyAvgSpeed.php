<?php


namespace TrafficManagement\users;

final class indexHourlyAvgSpeed {

    
    public function init($Table,$Type,$DType1,$DType2,$DType3,$Gtype) {
        // session
        //session_start();
        $_SESSION["table"] = $Table;
        $_SESSION["type"] = $Type;
        $_SESSION["Dtype1"] = $DType1;
        $_SESSION["Dtype2"] = $DType2;
        $_SESSION["Dtype3"] = $DType3;
        $_SESSION["Gtype"] = $Gtype;
    }

    /**
     * Run the application!
     */
    public function run() {
        $this->runPage($_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/View/VehicleHourlyAvgSpeed.phtml');
    }

    
    public function handleException($ex) {
        $extra = ['message' => $ex->getMessage()];
        if ($ex instanceof NotFoundException) {
            header('HTTP/1.0 404 Not Found');
            $this->runPage('404', $extra);
        } else {
            // TODO log exception
            header('HTTP/1.1 500 Internal Server Error');
            $this->runPage('500', $extra);
        }
    }

    
    public function loadClass($name) {
        if (!array_key_exists($name, self::$CLASSES)) {
            die('Class "' . $name . '" not found.');
        }
        require_once __DIR__ . self::$CLASSES[$name];
    }

    private function getPage() {
        $page = self::DEFAULT_PAGE;
        if (array_key_exists('page', $_GET)) {
            $page = $_GET['page'];
        }
        return $this->checkPage($page);
    }

    private function checkPage($page) {
        if (!preg_match('/^[a-z0-9-]+$/i', $page)) {
            // TODO log attempt, redirect attacker, ...
            throw new NotFoundException('Unsafe page "' . $page . '" requested');
        }
        if (!$this->hasScript($page)
                && !$this->hasTemplate($page)) {
            // TODO log attempt, redirect attacker, ...
            throw new NotFoundException('Page "' . $page . '" not found');
        }
        return $page;
    }

    private function runPage($page, array $extra = []) {
        //$run = false;
        //if ($this->hasScript($page)) {
        //    $run = true;
        //    require $this->getScript($page);
        //}
        //if ($this->hasTemplate($page)) {
        //    $run = true;
            // data for main template
            //$template = $this->getTemplate($page);
            //$flashes = null;
            //if (Flash::hasFlashes()) {
            //    $flashes = Flash::getFlashes();
            //}

            // main template (layout)
            require $_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/views/VehicleHourlyAvgSpeed.phtml';
            
        //}
        //if (!$run) {
        //    die('Page "' . $page . '" has neither script nor template!');
        //}
    }

    private function getScript($page) {
        return self::PAGE_DIR . $page . '.php';
    }

    private function getTemplate($page) {
        return self::PAGE_DIR . $page . '.phtml';
    }

    private function hasScript($page) {
        return file_exists($this->getScript($page));
    }

    private function hasTemplate($page) {
        return file_exists($this->getTemplate($page));
    }

}


