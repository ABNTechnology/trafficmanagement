<?php
namespace TrafficManagement\users\Mapper;

use TrafficManagement\users\Model\CustomerModel;

final class CustomerMapper {

    public function __construct() {
    }

    public static function map(CustomerModel $customerModel, array $properties) {
        if (array_key_exists('id', $properties)) {
            $customerModel->setId($properties['id']);
        }
        if (array_key_exists('FName', $properties)) {
            $customerModel->setFName($properties['FName']);
        }
        //return $customerModel;
    }

    private static function createDateTime($input) {
        return DateTime::createFromFormat('Y-n-j H:i:s', $input);
    }

}
