<?php
/*
UserSpice 4
An Open Source PHP User Management System
by the UserSpice Team at http://UserSpice.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
class Sensor {
	public $tableName = 'sensors';

        public
                            
			$_errors = [],
			$_db     = null;

	public function __construct($user = null){
		$this->_db = DB::getInstance();
		$this->_sessionName = Config::get('session/session_name');
		$this->_cookieName = Config::get('remember/cookie_name');


		if (!$user) {
			if (Session::exists($this->_sessionName)) {
				$user = Session::get($this->_sessionName);

				if ($this->find($user)) {
					$this->_isLoggedIn = true;
				} else {
					//process Logout
				}
			}
		} else {
			$this->find($user);
		}
	}
        
        public function updateSensor($fields = array(), $id=null){
		if (!$this->_db->UpdateSensor('sensors', $id, $fields)) {
			throw new Exception('There was a problem updating.');
		}
                return 1;
	}

	public function createSensor($fields = array()){
		if (!$this->_db->insert('sensors', $fields)) {
			throw new Exception('There was a problem creating an account.');
		}
		return 1;
	}

        public function find($user = null){
		if(isset($_SESSION['cloak_to'])){
			$user = $_SESSION['cloak_to'];
		}

		if ($user) {
			if(is_numeric($user)){
				$field = 'id';
			}elseif(!filter_var($user, FILTER_VALIDATE_EMAIL) === false){
				$field = 'email';
			}else{
				$field = 'username';
			}

			$data = $this->_db->get('users', array($field, '=', $user));

			if ($data->count()) {
				$this->_data = $data->first();
				if($this->data()->account_id == 0 && $this->data()->account_owner == 1){
					$this->_data->account_id = $this->_data->id;
				}
				return true;
			}
		}
		return false;
	}

        
	public function exists(){
		return (!empty($this->_data)) ? true : false;
	}

	public function data(){
		return $this->_data;
	}

	public function isLoggedIn(){
		return $this->_isLoggedIn;
	}


	//This is for future versions of UserSpice
	public function hasPermission($key){
		$group = $this->_db->get('permissions', array('id', '=', $this->data()->permissions));
		if ($group->count()) {
			$permissions = json_decode($group->first()->permissions, true);
			if ($permissions[$key] == true) {
				return true;
			}
		}
		return false;
	}
	//This is for future versions of UserSpice
	public function noPermissionRedirect($perm,$location){
		if(!$this->hasPermission($perm)){
			Redirect::to($location);
		}else{
			return true;
		}
	}

}
