<?php
/*
UserSpice 4
An Open Source PHP User Management System
by the UserSpice Team at http://UserSpice.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
?>
<?php require_once 'init.php'; ?>
<?php require_once $abs_us_root.$us_url_root.'users/includes/header.php'; ?>
<?php require_once $abs_us_root.$us_url_root.'users/includes/navigation.php'; ?>
<?php if (!securePage($_SERVER['PHP_SELF'])){die();}

$errors = [];
$successes = [];

if (Input::exists('get')) {
	$sensorsId=Input::get('sensorid');
	if (is_numeric($sensorsId) && $sensorsId>=0) {
		/*
		This is a valid ID so grab the record
		*/
		$item_results = $db->query("SELECT * FROM sensors WHERE sensorid=?",[$sensorsId]);
		$item = $item_results->first();
	}
}

$dropdown_results = $db->query("SELECT * FROM Locations WHERE IsActive=1");
$dropdowns = $dropdown_results->results();

$dropdown_Country_result = $db->query("SELECT * FROM countries WHERE IsActive=1");
$dropdowns_Country = $dropdown_Country_result->results();

$dropdown_Sensor_result = $db->query("SELECT * FROM sensortype");
$dropdowns_Sensor = $dropdown_Sensor_result->results();

   if (Input::exists('post')) {
    $sensor = new Sensor();
    $id=Input::get('SensorID');
    # Update the db with the new values
    $fields=array(
        'SensorID'=>$id,
        'SensorName'=>Input::get('SensorName'),
        'country'=>Input::get('country'),
        'state'=>Input::get('state'),
        'city'=>Input::get('city'),
        'sensortype'=>Input::get('sensortype'),
        'sensorlandmark'=>Input::get('sensorlandmark'),
        'latitude'=>Input::get('latitude'),
        'longitude'=>Input::get('longitude'),
         'mtc_no_start_time'=>Input::get('mtc_no_start_time'),
        'mtc_no_end_time'=>Input::get('mtc_no_end_time'),
        'mtc_max_height'=>Input::get('mtc_max_height'),
        'mtc_max_speed'=>Input::get('mtc_max_speed'),
        'mtc_allowed_headway'=>Input::get('mtc_allowed_headway'),
        'bus_no_start_time'=>Input::get('bus_no_start_time'),
        'bus_no_end_time'=>Input::get('bus_no_end_time'),
        'bus_max_height'=>Input::get('bus_max_height'),
        'bus_max_speed'=>Input::get('bus_max_speed'),
        'bus_allowed_headway'=>Input::get('bus_allowed_headway'),
        'car_no_start_time'=>Input::get('car_no_start_time'),
        'car_no_end_time'=>Input::get('car_no_end_time'),
        'car_max_height'=>Input::get('car_max_height'),
        'car_max_speed'=>Input::get('car_max_speed'),
        'car_allowed_headway'=>Input::get('car_allowed_headway'),
        'threewheeler_no_start_time'=>Input::get('threewheeler_no_start_time'),
        'threewheeler_no_end_time'=>Input::get('threewheeler_no_end_time'),
        'threewheeler_max_height'=>Input::get('threewheeler_max_height'),
        'threewheeler_max_speed'=>Input::get('threewheeler_max_speed'),
        'threewheeler_allowed_headway'=>Input::get('threewheeler_allowed_headway'),
        'lighttruck_no_start_time'=>Input::get('lighttruck_no_start_time'),
        'lighttruck_no_end_time'=>Input::get('lighttruck_no_end_time'),
        'lighttruck_max_height'=>Input::get('lighttruck_max_height'),
        'lighttruck_max_speed'=>Input::get('lighttruck_max_speed'),
        'lighttruck_allowed_headway'=>Input::get('lighttruck_allowed_headway'),
        'truck_no_start_time'=>Input::get('truck_no_start_time'),
        'truck_no_end_time'=>Input::get('truck_no_end_time'),
        'truck_max_height'=>Input::get('truck_max_height'),
        'truck_max_speed'=>Input::get('truck_max_speed'),
        'truck_allowed_headway'=>Input::get('truck_allowed_headway'),
        'locationtype'=>Input::get('locationtype'),
        'redirectscreen'=>Input::get('redirectscreen')
        
    );
   
     if ($sensor->updateSensor($fields,$id)) {

         Redirect::to('SensorsList.php?menu_title=&msg=Menu+item+updated');
    }
    else {
        Redirect::to('SensorsList.php?menu_title=&err=Unable+to+update+menu+item.');
    }
}

?>

              <div class="content-area">
                <div class="container">
                    <form class="needs-validation" method='post' action="SensorManagement_Edit.php" novalidate>
                       <div>
                            <div class="row">
                                <div class="col-md-8 order-md-1">
                                    <h4 class="mb-3">Sensor Details</h4>
                                   
                                        <div class="row">
                                            <div class="col-md-6 mb-3">
                                                <label for="SensorID">Sensor ID</label>
                                                <input type="text" class="form-control" readonly id="SensorID"  value='<?=$item->SensorID?>' name='SensorID' placeholder="Sensor ID">
                                                <div class="invalid-feedback">
                                                    Valid Sensor ID is required.
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="lastName">Sensor Name</label>
                                                <input type="text" class="form-control" name="SensorName" value='<?=$item->SensorName?>' id="SensorName" placeholder="Sensor Name" value="" required="">
                                                <div class="invalid-feedback">
                                                    Valid Sensor name is required.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 mb-3">
                                                <label for="country">Country</label>
                                                <select class="form-control" name="country" id="country" required="">
				<option value="">Choose...</option>
				<?php
				foreach ($dropdowns_Country as $dropdown_Country) {
				?>
                                        <option value="<?=$dropdown_Country->CountryID?>" <?php if ($item->country == $dropdown_Country->CountryID) echo 'selected="selected"'; ?> ><?=$dropdown_Country->CountryName?></option>
				<?php
				}
				?>
			</select>
                                                <div class="invalid-feedback">
                                                    Please select a valid country.
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="state">State</label>
                                                <select class="custom-select d-block w-100" id="state" required="" name="state">
                                                    <option value="">Choose...</option>
                                                    <option value="1" <?php if ($item->state == 1) echo 'selected="selected"'; ?> >Delhi</option>
                                                </select>
                                                <div class="invalid-feedback">
                                                    Please provide a valid state.
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="zip">City</label>
                                                <select class="custom-select d-block w-100" id="city" required="" name="city">
                                                    <option value="">Choose...</option>
                                                    <option value="1" <?php if ($item->city == 1) echo 'selected="selected"'; ?> >Delhi</option>
                                                </select>
                                                <div class="invalid-feedback">
                                                    Please provide a valid City.
                                                </div>
                                            </div>
                                        </div>
 <div class="row">
                                            <div class="col-md-6 mb-3">
                                                <label for="sensortype">Sensor Type</label>
                                                <select class="custom-select d-block w-100" id="sensortype" required="" name="sensortype">
                                                          <option value="">Choose...</option>
				<?php
				foreach ($dropdowns_Sensor as $dropdown_Sensor) {
				?>
                                        <option value="<?=$dropdown_Sensor->SensorTypeID?>" <?php if ($item->sensortype == $dropdown_Sensor->SensorTypeID) echo 'selected="selected"'; ?> ><?=$dropdown_Sensor->SensorTypeValue?></option>
				<?php
				}
				?>
                                                </select>
                                                <div class="invalid-feedback">
                                                    Valid Sensor Type is required.
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="SensorLandmark">Sensor Landmark</label>
                                                <input type="text" class="form-control" id="sensorlandmark" value='<?=$item->sensorlandmark?>' name="sensorlandmark" placeholder="Enter Landmark" value="" required="">
                                                <div class="invalid-feedback">
                                                    Valid Sensor Landmark is required.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 mb-3">
                                                <label for="laititude">Latitude</label>
                                                <input type="number" class="form-control" id="latitude" value='<?=$item->latitude?>' name="latitude" placeholder="Enter Latitude" value="" required="">
                                                <div class="invalid-feedback">
                                                    Valid Latitude is required.
                                                </div>
                                            </div>
                                            <div class="col-md-6 mb-3">
                                                <label for="number">Longitude</label>
                                                <input type="text" class="form-control" id="longitude" name="longitude" value='<?=$item->longitude?>' placeholder="Enter Longitude" value="" required="">
                                                <div class="invalid-feedback">
                                                    Valid Longitude is required.
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 mb-3">
                                                <label for="MOTORCYCLE">Vehicle Type</label>
                                                <select class="form-control" name="MOTORCYCLE">
				<option value="1">MOTORCYCLE</option>
				
			</select>
                                                <div class="invalid-feedback">
                                                    Please select a valid Vehicle Type.
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="mtc_no_start_time">No Entry Start Time</label>
                                                <input type="text" class="form-control" id="mtc_no_start_time" name="mtc_no_start_time"  value='<?=$item->mtc_no_start_time?>' required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid Time.
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="mtc_no_end_time">No Entry End Time</label>
                                                <input type="text" class="form-control" id="mtc_no_end_time" name="mtc_no_end_time" value='<?=$item->mtc_no_end_time?>' required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid Time.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 mb-3">
                                                <label for="mtc_max_speed">Maximum Speed Allowed</label>
                                                <input type="number" class="custom-select d-block w-100" id="mtc_max_speed" value='<?=$item->mtc_max_speed?>' name="mtc_max_speed" required="">
                                                <div class="invalid-feedback">
                                                    Please select Maximum Speed.
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="mtc_max_height">Maximum Height</label>
                                                <input type="number" class="form-control" id="mtc_max_height" name="mtc_max_height" value='<?=$item->mtc_max_height?>' placeholder="Enter Maximum Height" required="">
                                                <div class="invalid-feedback">
                                                    Please provide Maximum Height.
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="mtc_allowed_headway">Allowed headway</label>
                                                <input type="number" class="form-control" id="mtc_allowed_headway" name="mtc_allowed_headway" value='<?=$item->mtc_allowed_headway?>' placeholder="Enter Allowed Headway" required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid allowed headway.
                                                </div>
                                            </div>
                                        </div>
                                     <div class="row">
                                            <div class="col-md-5 mb-3">
                                                <label for="BUS">Vehicle Type</label>
                                                 <select class="form-control" name="BUS">
				<option value="">BUS</option>
					</select>
                                                <div class="invalid-feedback">
                                                    Please select a valid Vehicle Type.
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="bus_no_start_time">No Entry Start Time</label>
                                                <input type="text" class="form-control" id="bus_no_start_time" name="bus_no_start_time" value='<?=$item->bus_no_start_time?>' required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid Time.
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="bus_no_end_time">No Entry End Time</label>
                                                <input type="text" class="form-control" id="bus_no_end_time" name="bus_no_end_time" value='<?=$item->bus_no_end_time?>' required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid Time.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 mb-3">
                                               <label for="bus_max_speed">Maximum Speed Allowed</label>
                                                <input type="number" class="custom-select d-block w-100" id="bus_max_speed" value='<?=$item->bus_max_speed?>' name="bus_max_speed" required="">
                                                <div class="invalid-feedback">
                                                    Please select Maximum Speed.
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="bus_max_height">Maximum Height</label>
                                                <input type="number" class="form-control" id="bus_max_height" name="bus_max_height" value='<?=$item->bus_max_height?>' placeholder="Enter Maximum Height" required="">
                                                <div class="invalid-feedback">
                                                    Please provide Maximum Height.
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="bus_allowed_headway">Allowed headway</label>
                                                <input type="number" class="form-control" id="bus_allowed_headway" name="bus_allowed_headway" value='<?=$item->bus_allowed_headway?>' placeholder="Enter Allwoed Headway" required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid allowed headway.
                                                </div>
                                            </div>
                                        </div>
                                     <div class="row">
                                            <div class="col-md-5 mb-3">
                                                <label for="CAR">CAR</label>
                                                <select class="form-control" name="CAR"> <option value="">CAR</option>
                                                </select>          
                                                <div class="invalid-feedback">
                                                    Please select a valid Vehicle Type.
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="car_no_start_time">No Entry Start Time</label>
                                                <input type="text" class="form-control" name="car_no_start_time" id="car_no_start_time" value='<?=$item->car_no_start_time?>' required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid Time.
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="car_no_end_time">No Entry End Time</label>
                                                <input type="text" class="form-control" id="car_no_end_time" name="car_no_end_time" value='<?=$item->car_no_end_time?>' required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid Time.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 mb-3">
                                                <label for="car_max_speed">Maximum Speed Allowed</label>
                                                <input type="number" class="custom-select d-block w-100" id="car_max_speed" value='<?=$item->car_max_speed?>' name="car_max_speed" required="">
                                                <div class="invalid-feedback">
                                                    Please select Maximum Speed.
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="car_max_height">Maximum Height</label>
                                                <input type="number" class="form-control" id="car_max_height" name="car_max_height" value='<?=$item->car_max_height?>' placeholder="Enter Maximum Height" required="">
                                                <div class="invalid-feedback">
                                                    Please provide Maximum Height.
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="car_allowed_headway">Allowed headway</label>
                                                <input type="number" class="form-control" id="car_allowed_headway" name="car_allowed_headway" value='<?=$item->car_allowed_headway?>' placeholder="Enter Allwoed Headway" required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid Allowed headway.
                                                </div>
                                            </div>
                                        </div>
                                     <div class="row">
                                            <div class="col-md-5 mb-3">
                                                
                                                 <label for="THREEWHEELER">Vehicle Type</label>
                                                <select class="form-control" name="THREEWHEELER">
				<option value="">THREE WHEELER</option>
				
			</select>
                                                <div class="invalid-feedback">
                                                    Please select a valid Vehicle Type.
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="threewheeler_no_start_time">No Entry Start Time</label>
                                                <input type="text" class="form-control" id="threewheeler_no_start_time" name="threewheeler_no_start_time" value='<?=$item->threewheeler_no_start_time?>' required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid Time.
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="threewheeler_no_end_time">No Entry End Time</label>
                                                <input type="text" class="form-control" id="threewheeler_no_end_time" name="threewheeler_no_end_time" value='<?=$item->threewheeler_no_end_time?>' required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid Time.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 mb-3">
                                               <label for="threewheeler_max_speed">Maximum Speed Allowed</label>
                                                <input type="number" class="custom-select d-block w-100" id="threewheeler_max_speed" value='<?=$item->threewheeler_max_speed?>' name="threewheeler_max_speed" required="">
                                                <div class="invalid-feedback">
                                                    Please select Maximum Speed.
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="threewheeler_max_height">Maximum Height</label>
                                                <input type="number" class="form-control" id="threewheeler_max_height" name="threewheeler_max_height" value='<?=$item->threewheeler_max_height?>' placeholder="Enter Maximum Height" required="">
                                                <div class="invalid-feedback">
                                                    Please enter maximum Height.
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="threewheeler_allowed_headway">Allowed headway</label>
                                                <input type="number" class="form-control" id="threewheeler_allowed_headway" name="threewheeler_allowed_headway" value='<?=$item->threewheeler_allowed_headway?>' placeholder="Enter Allowed Headway" required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid allowed headway.
                                                </div>
                                            </div>
                                        </div>
                                     <div class="row">
                                            <div class="col-md-5 mb-3">
                                                 <label for="LIGHTTRUCK">Vehicle Type</label>
                                                <select class="form-control" name="LIGHTTRUCK">
				<option value="">LIGHT TRUCK</option>
			</select>
                                                <div class="invalid-feedback">
                                                    Please select a valid Vehicle Type.
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="lighttruck_no_start_time">No Entry Start Time</label>
                                                <input type="text" class="form-control" id="lighttruck_no_start_time" name="lighttruck_no_start_time" value='<?=$item->lighttruck_no_start_time?>' required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid Time.
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="lighttruck_no_end_time">No Entry End Time</label>
                                                <input type="text" class="form-control" id="lighttruck_no_end_time" name="lighttruck_no_end_time" value='<?=$item->lighttruck_no_end_time?>' required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid Time.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 mb-3">
                                                <label for="lighttruck_max_speed">Maximum Speed Allowed</label>
                                                <input type="number" class="custom-select d-block w-100" id="lighttruck_max_speed" value='<?=$item->lighttruck_max_speed?>' name="lighttruck_max_speed" required="">
                                                <div class="invalid-feedback">
                                                    Please select Maximum Speed.
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="lighttruck_max_height">Maximum Height</label>
                                                <input type="number" class="form-control" id="lighttruck_max_height" name="lighttruck_max_height" value='<?=$item->lighttruck_max_height?>' placeholder="Enter Maximum Height" required="">
                                                <div class="invalid-feedback">
                                                    Please provide maximum Height.
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="lighttruck_allowed_headway">Allowed headway</label>
                                                <input type="number" class="form-control" id="lighttruck_allowed_headway" name="lighttruck_allowed_headway" value='<?=$item->lighttruck_allowed_headway?>' placeholder="Enter Allowed Headway" required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid allowed headway.
                                                </div>
                                            </div>
                                        </div>
                                        
                                         <div class="row">
                                            <div class="col-md-5 mb-3">
                                                 <label for="TRUCK">Vehicle Type</label>
                                                <select class="form-control" name="TRUCK">
				<option value="">TRUCK</option>
			</select>
                                                <div class="invalid-feedback">
                                                    Please select a valid Vehicle Type.
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="truck_no_start_time">No Entry Start Time</label>
                                                <input type="text" class="form-control" id="truck_no_start_time" name="truck_no_start_time" value='<?=$item->truck_no_start_time?>' required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid Time.
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="truck_no_end_time">No Entry End Time</label>
                                                <input type="text" class="form-control" id="truck_no_end_time" name="truck_no_end_time" value='<?=$item->truck_no_end_time?>' required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid Time.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5 mb-3">
                                                <label for="truck_max_speed">Maximum Speed Allowed</label>
                                               <input type="number" class="custom-select d-block w-100" id="truck_max_speed" value='<?=$item->truck_max_speed?>' name="truck_max_speed" required="">
                                                <div class="invalid-feedback">
                                                    Please select maximum speed.
                                                </div>
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="truck_max_height">Maximum Height</label>
                                                <input type="number" class="form-control" id="truck_max_height" name="truck_max_height" value='<?=$item->truck_max_height?>' placeholder="Enter Maximum Height" required="">
                                                <div class="invalid-feedback">
                                                    Please provide a maximum height.
                                                </div>
                                            </div>
                                            <div class="col-md-3 mb-3">
                                                <label for="truck_allowed_headway">Allowed headway</label>
                                                <input type="number" class="form-control" id="truck_allowed_headway" name="truck_allowed_headway" value='<?=$item->truck_allowed_headway?>' placeholder="Enter Allowed Headway" required="">
                                                <div class="invalid-feedback">
                                                    Please provide a valid Allowed Headway.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="LocationType">Location Type</label>
                                            <div class="input-group">
                                                 <select class="form-control" id="LocationType" name="locationtype">
                                               <option value="">Option..</option>
				<?php
				foreach ($dropdowns as $dropdown) {
				?>
                                        <option value="<?=$dropdown->LocationID?>" <?php if ($item->locationtype == $dropdown->LocationID) echo 'selected="selected"'; ?> ><?=$dropdown->LocationName?></option>
				<?php
				}
                                ?></select>
                                                <div class="invalid-feedback" style="width: 100%;">
                                                    Your username is required.
                                                </div>
                                            </div>
                                        </div>

                                        <div class="mb-3">
                                            <label for="redirectscreen">Redirect Screen <span class="text-muted">(Optional)</span></label>
                                            <input type="text" class="form-control" id="RedirectScreen" name="redirectscreen" value='<?=$item->redirectscreen?>' placeholder="Screen Direction To Screen">
                                            <div class="invalid-feedback">
                                                Please enter a valid Screen Direction.
                                            </div>
                                        </div>

                                        <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to Save</button>
                                    </form>
                                </div>
                            </div>
                       </div>
                   </form>    
                </div>
            </div>
<!-- footers -->
<?php require_once $abs_us_root.$us_url_root.'users/includes/page_footer.php'; // the final html footer copyright row + the external js calls ?>

<!-- Place any per-page javascript here -->

<?php require_once $abs_us_root.$us_url_root.'users/includes/html_footer.php'; // currently just the closing /body and /html ?>
 <script>
			$(function() {
				$('#mtc_no_start_time').timepicker({ 'timeFormat': 'H:i:s' });
                                $('#mtc_no_end_time').timepicker({ 'timeFormat': 'H:i:s' });
                                $('#bus_no_start_time').timepicker({ 'timeFormat': 'H:i:s' });
                                $('#bus_no_end_time').timepicker({ 'timeFormat': 'H:i:s' });
                                $('#car_no_start_time').timepicker({ 'timeFormat': 'H:i:s' });
                                $('#car_no_end_time').timepicker({ 'timeFormat': 'H:i:s' });
                                $('#threewheeler_no_start_time').timepicker({ 'timeFormat': 'H:i:s' });
                                $('#threewheeler_no_end_time').timepicker({ 'timeFormat': 'H:i:s' });
                                $('#lighttruck_no_start_time').timepicker({ 'timeFormat': 'H:i:s' });
                                $('#lighttruck_no_end_time').timepicker({ 'timeFormat': 'H:i:s' });
                                $('#truck_no_start_time').timepicker({ 'timeFormat': 'H:i:s' });
                                $('#truck_no_end_time').timepicker({ 'timeFormat': 'H:i:s' });
                                
			});
			</script>
<script>     

    
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>  
    
    <link href="css/jquery.timepicker.css" rel="stylesheet" type="text/css"/>
    <link href="css/jquery.timepicker.min.css" rel="stylesheet" type="text/css"/>
    <a href="js/plugins/DateTime/GruntFile.js"></a>
    <script src="js/plugins/DateTime/jquery.timepicker.js" type="text/javascript"></script>
    <script src="js/plugins/DateTime/jquery.timepicker.min.js" type="text/javascript"></script>