<?php
//namespace SalesCRM;

use \TrafficManagement\users\Model\Customer;
use \TrafficManagement\users\Utils\Utils;
use \TrafficManagement\users\Config\Config;

if(isset($_GET['controller']) && !empty($_GET['controller'])){
     $controller =$_GET['controller'];
}else{
     $controller ='CustomerController'; 
}

if(isset($_GET['function']) && !empty($_GET['function'])){
    $function =$_GET['function'];
}else{
    $function ='Listing'; 
}


$controller=$controller;
$fn = $_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/Controller/'.$controller . '.php';
if(file_exists($fn)){
    require_once($fn);
    $controllerClass=$controller;
    if(!method_exists($controllerClass,$function)){
       die($function .' Action not found');
    }
    $obj=new $controllerClass;
    
    echo 'Local DB call: ';
    $obj-> $function();
    
    //API CALL
    
    $name = "?Controller=CustomerAPIController&Function=listing";
    require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/Config/Config.php');
        
    $config=new Config();
    $configdata=$config->getConfig('APIURL');
    $url = $configdata['url'];
    $url = $url.$name;
    $result = file_get_contents($url);
    //$curl = curl_init();
    //curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    
    //curl_setopt($curl, CURLOPT_URL, $url);
    //curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    //$result = curl_exec($curl);

    //curl_close($curl);

    //var_dump($result);
    $arr=json_decode($result, true);
    //var_dump(json_decode($result, true));
    //print_r($arr);
    //$result = json_decode($result,TRUE);
    echo '</br>';
    
    echo '</br>';
    echo 'Web Api call: ';
    echo $arr['status'];
    echo $arr['data'][0];
    echo $arr['data'][1];
    echo '</br>';
    echo '</br>';
}else{
    die($fn  .' Controller not found');
}

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js'></script>


<script type="text/javascript">    
function ajaxtest()
{
        $.ajax({
            type: "GET",
            url: 'http://localhost/TrafficManagementAPI',
            data :  {Controller: 'CustomerAPIController',Function: 'Listing'},
            dataType: 'json',
            success: function(response){
                //var len = response.length;
                
                //for(var i=0; i<len; i++){
                var username = response.data[0];
                var tr_str = "<tr>" +
                     "<td align='center'>1</td>" +
                    "<td align='center'>" + username + "</td>" +
                    "</tr>";
                
                $("#userTable").append(tr_str);
                
                var username1 = response.data[1];
                var tr_str1 = "<tr>" +
                     "<td align='center'>1</td>" +
                    "<td align='center'>" + username1 + "</td>" +
                    "</tr>";
                
                $("#userTable").append(tr_str1);
            }
        });
}

</script>
    </head>
    <body>
        <form action="index.php" method='POST'>
        <?php
        if (isset($_POST['select'])) {
            
            switch ($_POST['select']) {
                case 'insert':
                    insert();
                    break;
                case 'select':
                    select();
                    break;
            }
        }

        function select() {
            echo "The select function is called.";
            exit;
        }

        function insert() {
            echo "The insert function is called.";
            exit;
        }
        //include "testgraph.php";
        //echo ' API Call: '. $result['data'];
        require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/IndexNew.php');
        //$testGraph=new TestGraph('test','test1','test2','test3','test4');
        //include "testgraph.php";

        $index = new \TrafficManagement\users\indexnew();
        $index->init('vehiclettansactions','Multiple','Hour','Class','Count','Line');
        // run application!
        $index->run();
?>
        <div class="container">
            <table id="userTable" border="2" >
               <tr>
                <th width="5%">S.no</th>
                <th width="95%">Name -JQuery Ajax call</th>
               </tr>
            </table>
         </div>

        <input type="submit" class="button" name="select" value="select" />
        <input type="submit" class="button" name="select" value="insert" />
        <input type="button" class="button1" name="select" value="AjaxCall1" />
        <input type="button" name="Test" value="AjaxCall2" onclick="ajaxtest();"/>
        </form>
    </body>
    
    
</html>
