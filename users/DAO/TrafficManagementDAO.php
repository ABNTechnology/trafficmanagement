<?php

use \TrafficManagement\users\Controller\CustomerController;
use \TrafficManagement\users\Model\CustomerModel;
use \TrafficManagement\users\Mapper\CustomerMapper;
use \TrafficManagement\users\Utils\Utils;
use \TrafficManagement\users\Config\Config;

final class TrafficManagementDAO {

    /** @var PDO */ 
    private $db = null;


    public function __destruct() {
        // close db connection
        $this->db = null;
    }

    
    /**
     * Find by identifier.
     * @return list or <i>null</i> if not found
     */
    public function findById($id) {
        $row = $this->query('SELECT * FROM customer WHERE id = ' . (int) $id)->fetch();
        if (!$row) {
            return null;
        }
        
        require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/Mapper/CustomerMapper.php');
      
        $customerMapper = new CustomerMapper();
        
        require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/Model/CustomerModel.php');
        
        $customerModel = new CustomerModel();
        $customerModel=$customerMapper->map($customerModel, $row);
        //SalesCRM\Mapper\CustomerMapper::map($customerModel, $row);
        return $customerModel;
    }
    public function find() {
                
        $rows=$this->query('SELECT * FROM customer where id in (1,2)');
        $result = [];
        $i=0;
        require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/Model/CustomerModel.php');
        $MyObject = new CustomerModel();   
        $MyObjects = array();
        require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/Mapper/CustomerMapper.php');

        $customerMapper = new CustomerMapper();

        foreach ($rows as $row) {
        $MyObject = new CustomerModel();
        $customerMapper::map($MyObject, $row);           
        $MyObjects[] = $MyObject;
        }

        return $MyObjects;
    }
    public function save(CustomerModel $customerModel) {
        return $this->insert($customerModel);
        
    }

    public function delete($id) {
        $sql = '
            delete customer 
            WHERE
                id = :id';
        $statement = $this->getDb()->prepare($sql);
        $this->executeStatement($statement, [
            ':id' => $id,
        ]);
        return $statement->rowCount() == 1;
    }

    /**
     * @return PDO
     */
    private function getDb() {
        if ($this->db !== null) {
            return $this->db;
        }
        
        require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/Config/Config.php');
        
        $config=new Config();
        $configdata=$config->getConfig('db');
         $servername = $configdata['servername'];
        $username =  $configdata['username'];
        $password =  $configdata['password'];
        $dbname= $configdata['dbname'];;
        try {
            $this->db = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        } catch (Exception $ex) {
            throw new Exception('DB connection error: ' . $ex->getMessage());
        }
        return $this->db;
    }

    
    private function insert(CustomerModel $customerModel) {
        
        $id=$customerModel->getId();
        $fname=$customerModel->getFName();
        $sql = '
            INSERT INTO customer (id, FName)
                VALUES ($id, $fname)';
        return $this->execute($sql, $customerModel);
    }

    
    private function update(CustomerModel $customerModel) {
        
        $sql = '
            UPDATE todo SET
                FName = :FName
            WHERE
                id = :id';
        return $this->execute($sql, $customerModel);
    }

    private function execute($sql, CustomerModel $customerModel) {
        $statement = $this->getDb()->prepare($sql);
        $this->executeStatement($statement, $this->getParams($customerModel));
        
        return $customerModel;
    }

    private function getParams(CustomerModel $customerModel) {
        $params = [
            ':Id' => $customerModel->getId(),
            ':FName' => $customerModel->getFName(),
        ];
        
        return $params;
    }

    private function executeStatement(PDOStatement $statement, array $params) {
        // XXX
        //echo str_replace(array_keys($params), $params, $statement->queryString) . PHP_EOL;
        if ($statement->execute($params) === false) {
            self::throwDbError($this->getDb()->errorInfo());
        }
    }

    /**
     * @return PDOStatement
     */
    private function query($sql) {
        $statement = $this->getDb()->query($sql, PDO::FETCH_ASSOC);
        if ($statement === false) {
            self::throwDbError($this->getDb()->errorInfo());
        }
        return $statement;
    }

    private function getFindSql() {
        $sql = 'SELECT * FROM customer order by id asc';
        
        return $sql;
    }
    
    private static function throwDbError(array $errorInfo) {
        // TODO log error, send email, etc.
        throw new Exception('DB error [' . $errorInfo[0] . ', ' . $errorInfo[1] . ']: ' . $errorInfo[2]);
    }

    private static function formatDateTime(DateTime $date) {
        return $date->format('Y-m-d H:i:s');
    }

    private static function formatBoolean($bool) {
        return $bool ? 1 : 0;
    }

}
