<?php
/*
UserSpice 4
An Open Source PHP User Management System
by the UserSpice Team at http://UserSpice.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
?>
<?php require_once 'init.php'; ?>
<?php require_once $abs_us_root.$us_url_root.'users/includes/header.php'; ?>
<?php require_once $abs_us_root.$us_url_root.'users/includes/navigation.php'; ?>

<?php if (!securePage($_SERVER['PHP_SELF'])){die();}?>
<?php
if(!empty($_POST['uncloak'])){
	if(isset($_SESSION['cloak_to'])){
		$to = $_SESSION['cloak_to'];
		$from = $_SESSION['cloak_from'];
		unset($_SESSION['cloak_to']);
		$_SESSION['user'] = $_SESSION['cloak_from'];
		unset($_SESSION['cloak_from']);
		logger($from,"Cloaking","uncloaked from ".$to);
		Redirect::to('admin_users.php?err=You+are+now+you!');
		}else{
			Redirect::to('logout.php?err=Something+went+wrong.+Please+login+again');
		}
}


//dealing with if the user is logged in
if($user->isLoggedIn() || !$user->isLoggedIn() && !checkMenu(2,$user->data()->id)){
	if (($settings->site_offline==1) && (!in_array($user->data()->id, $master_account)) && ($currentPage != 'login.php') && ($currentPage != 'maintenance.php')){
		$user->logout();
		Redirect::to($us_url_root.'users/maintenance.php');
	}
}
$grav = get_gravatar(strtolower(trim($user->data()->email)));
$get_info_id = $user->data()->id;
// $groupname = ucfirst($loggedInUser->title);
$raw = date_parse($user->data()->join_date);
$signupdate = $raw['month']."/".$raw['day']."/".$raw['year'];
$userdetails = fetchUserDetails(NULL, NULL, $get_info_id); //Fetch user details


 ?>

                                      <div id="page-wrapper" class="content-area">
<div class="container">
<div class="space-top space-bottom">
                                              <article class="article border-bottom">
                                                  <div class="container">
                                                      <div class="content">
                                                          <h2 class="heading-text">Vehicle Distribution by Type</h2>
                                                          <p> Today as of now <strong><label id="lblCarPercentageDesc"></label><span  class="percentage">%</span></strong> of the vehicles passed were Cars along with MotorCycles which constitutes <strong><label id="lblMotoryclePercentageDesc"></label> <span class="percentage">%</span></strong> of vehicles. Trucks were around <strong><label id="lblTruckPercentageDesc"></label><span class="percentage">%</span></strong>. </p>
                                                      </div>
                                                      <div class="icons-container">
                                                          <ul class="list-inline">
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-scooter icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          <label id="lblMotoryclePercentage"></label> <span class="percentage">%</span>
                                                                      </div>
                                                                      <div class="dashed-bottom">Motorcycle<br><label id="lbltotalMotorycle"></label></div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-auto-rickshaw icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          <label id="lblThreeWheelerPercentage"/><span class="percentage">%</span>
                                                                      </div>
                                                                      <div class="dashed-bottom">Three wheeler<br><label id="lbltotalThreeWheeler"></label></div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-car icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          <label id="lblCarPercentage"/><span class="percentage">%</span>
                                                                      </div>
                                                                      <div class="dashed-bottom">Car<br><label id="lbltotalCar"></label></div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-bus icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          <label id="lblBusPercentage"/><span class="percentage">%</span>
                                                                      </div>
                                                                      <div class="dashed-bottom">Bus<br><label id="lbltotalBus"></label></div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-light-truck icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          <label id="lblLightTruckPercentage"/><span class="percentage">%</span>
                                                                      </div>
                                                                      <div class="dashed-bottom">Light Truck<br><label id="lbltotalLightTruck"></label></div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-truck icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          <label id="lblTruckPercentage"/><span class="percentage">%</span>
                                                                      </div>
                                                                      <div class="dashed-bottom">Truck<br><label id="lbltotalTruck"></label></div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                          </ul>
                                                      </div>
                                                      <div class="graph" style="height:250px; width:100%;">
                                                        <?php
                                                      require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/IndexCurrentStatus.php');
                                                              //$testGraph=new TestGraph('test','test1','test2','test3','test4');
                                                              //include "testgraph.php";

                                                              $index1 = new \TrafficManagement\users\IndexCurrentStatus();
                                                              $index1->init('sp_VehicleCurrentCount','Multiple','Class','None','Count','Column');
                                                              // run application!
                                                              $index1->run();
                                                      ?>
                                                                         
                                                    </div>
                                                  </div>
                                              </article>
                                              <article class="article border-bottom">
                                                  <div class="container">
                                                      <div class="content">
                                                          <h2 class="heading-text">Hourly Count by Vehicle Type</h2>
                                                          <p> Today as of now, <strong style="color:red;"> Count of Motorcycle %age is high </strong> and 2nd highest is the LightTruck. <strong style="color:Orange;">No Heavy Truck in last one hour </strong> Car vount is average <strong style="color:Green;"> No traffic surge in last one hour. </p>
                                                      </div>
                                                      <div class="icons-container">
                                                          <ul class="list-inline">
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-scooter icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          30<span class="kmh"> Motor Cycles</span>
                                                                          <i class="icon-flag icon-md text-danger"></i>
                                                                      </div>
                                                                      
                                                                  </aside>
                                                                  
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-auto-rickshaw icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          20<span class="kmh">Three Wheelers</span>
                                                                          <i class="icon-flag icon-md text-warning"></i>
                                                                      </div>
                                                                      
                                                                  </aside>
                                                                  
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-car icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          10<span class="kmh">Cars</span>
                                                                          <i class="icon-flag icon-md text-success"></i>
                                                                      </div>
                                                                      
                                                                  </aside>
                                                                  
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-bus icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          15<span class="kmh">Buses</span>
                                                                          <i class="icon-flag icon-md text-success"></i>
                                                                      </div>
                                                                      
                                                                  </aside>
                                                                  
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-light-truck icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          35<span class="kmh"> Light Truck</span>
                                                                          <i class="icon-flag icon-md text-danger"></i>
                                                                      </div>
                                                                      
                                                                  </aside>
                                                                  
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-truck icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          25<span class="kmh">Heavy Trucks</span>
                                                                          <i class="icon-flag icon-md text-danger"></i>
                                                                      </div>
                                                                      
                                                                  </aside>
                                                                  
                                                              </li>
                                                          </ul>
                                                      </div>
                                                      <div class="graph">
                                                                    <?php
                                                                    require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/IndexHourlyCount.php');
                                                                            //$testGraph=new TestGraph('test','test1','test2','test3','test4');
                                                                            //include "testgraph.php";

                                                                            $index2 = new \TrafficManagement\users\IndexHourlyCount();
                                                                            $index2->init('sp_VehicleDataHourlyCount','Single','Hour','Class','Count','Line');
                                                                            // run application!1.1
                                                                            
                                                                            $index2->run();
                                                                    ?>
                                                                    
                                                                  </div>
                                              </article>
                                              <article class="article border-bottom">
                                                  <div class="container">
                                                      <div class="content">
                                                          <h2 class="heading-text">Average Speed by Vehicle Type</h2>
                                                          <p> Today as of now, <strong style="color:red;"> Motorcycle @ 50kmph , Light Truck @ 35 KMPH and Trucks @ 25 KMPH </strong> were running above specified speed. <strong style="color:Orange;">Three Wheeler @ 40 KMPH </strong>are running Marginally above specified limit <strong style="color:Green;">Cars @ 30 KMPh and Bus @ 25KMPH </strong> are running below specified limit. </p>
                                                      </div>
                                                      <div class="icons-container">
                                                          <ul class="list-inline">
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-scooter icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          50<span class="kmh">km/h</span>
                                                                          <i class="icon-flag icon-md text-danger"></i>
                                                                      </div>
                                                                      <div style="color:ROYALBLUE;"><STRONG>SPECIFIED RANGE</STRONG></div>
                                                                      <div class="dashed-bottom">45 KMPH</div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-auto-rickshaw icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          40<span class="kmh">km/h</span>
                                                                          <i class="icon-flag icon-md text-warning"></i>
                                                                      </div>
                                                                      <div style="color:ROYALBLUE;"><STRONG>SPECIFIED RANGE</STRONG></div>
                                                                      <div class="dashed-bottom">40 KMPH</div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-car icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          30<span class="kmh">km/h</span>
                                                                          <i class="icon-flag icon-md text-success"></i>
                                                                      </div>
                                                                      <div style="color:ROYALBLUE;"><STRONG>SPECIFIED RANGE</STRONG></div>
                                                                      <div class="dashed-bottom">35 KMPH</div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-bus icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          25<span class="kmh">km/h</span>
                                                                          <i class="icon-flag icon-md text-success"></i>
                                                                      </div>
                                                                      <div style="color:ROYALBLUE;"><STRONG>SPECIFIED RANGE</STRONG></div>
                                                                      <div class="dashed-bottom">30 KMPH</div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-light-truck icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          35<span class="kmh">km/h</span>
                                                                          <i class="icon-flag icon-md text-danger"></i>
                                                                      </div>
                                                                      <div style="color:ROYALBLUE;"><STRONG>SPECIFIED RANGE</STRONG></div>
                                                                      <div class="dashed-bottom">30 KMPH</div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-truck icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          25<span class="kmh">km/h</span>
                                                                          <i class="icon-flag icon-md text-danger"></i>
                                                                      </div>
                                                                      <div style="color:ROYALBLUE;"><STRONG>SPECIFIED RANGE</STRONG></div>
                                                                      <div class="dashed-bottom">20 KMPH</div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                          </ul>
                                                      </div>
                                        
                                                      <div class="graph">
                                                                    
                                                                    <?php
                                                                    require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagement/users/IndexHourlyAvgSpeed.php');
                                                                            //$testGraph=new TestGraph('test','test1','test2','test3','test4');
                                                                            //include "testgraph.php";

                                                                            $index2 = new \TrafficManagement\users\IndexHourlyAvgSpeed();
                                                                            $index2->init('sp_VehicleHourlyAvgSpeed','Single','Hour','Class','Speed','Bar');
                                                                            // run application!
                                                                            $index2->run();
                                                                    ?>
                                                                  </div>
                                                  </div>
                                              </article>
                                              <div class="banner-panel">
                                                  <article class="article">
                                                      <div class="content">
                                                          <h2 class="heading-text">IOT TRAFFIC MONITORING SYSTEM</h2>
                                                          <p>An Internet of things (IOT) based wireless sensor system, solely using wireless accelerometers, is developed for traffic volume and vehicle classification monitoring in this paper. A series of laboratory tests, field tests as well as numerical simulation were performed to validate the feasibility and accuracy of the monitoring system. Besides, in order to eliminate the impacts of noises in the output signals, an advanced algorithm is developed to analyze the test data. The findings based on the test results indicate that the system is capable of reliably detecting axles and calculating axle spacing in both laboratory and field tests. In addition, compared with the actual measurements, the numerical simulation further validates the feasibility of the integrated wireless sensor system for traffic information monitoring.</p>
                                                          <!-- <p class="pt-4 mb-0"><a href="#" class="btn">Sign Up</a></p> -->
                                                      </div>
                                                  </article>
                                              </div>
                                              <article class="article border-bottom">
                                                  <div class="container">
                                                      <div class="content">
                                                          <h2 class="heading-text">Violation % by class</h2>
                                                          <p>% of vehicles of a specific class who had violated the rule for any reason. Except Cars rest of the vehicle type violation % has gone up against till date violation%. </p>
                                                      </div>
                                                      <div class="icons-container">
                                                          <ul class="list-inline">
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-scooter icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          25<span class="percentage">%</span>
                                                                          <i class="icon-flag icon-md text-danger"></i>
                                                                      </div>
                                                                      <div style="color:ROYALBLUE;"><STRONG>Violation % till date</STRONG></div>
                                                                      <div class="dashed-bottom">20 <span class="percentage">%</span></div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-auto-rickshaw icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          25<span class="percentage">%</span>
                                                                          <i class="icon-flag icon-md text-warning"></i>
                                                                      </div>
                                                                      <div style="color:ROYALBLUE;"><STRONG>Violation % till date</STRONG></div>
                                                                      <div class="dashed-bottom">25 <span class="percentage">%</span></div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-car icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          25<span class="percentage">%</span>
                                                                          <i class="icon-flag icon-md text-success"></i>
                                                                      </div>
                                                                      <div style="color:ROYALBLUE;"><STRONG>Violation % till date</STRONG></div>
                                                                      <div class="dashed-bottom">30 <span class="percentage">%</span></div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-bus icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          25<span class="percentage">%</span>
                                                                          <i class="icon-flag icon-md text-danger"></i>
                                                                      </div>
                                                                      <div style="color:ROYALBLUE;"><STRONG>Violation % till date</STRONG></div>
                                                                      <div class="dashed-bottom">20 <span class="percentage">%</span></div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-light-truck icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          25<span class="percentage">%</span>
                                                                          <i class="icon-flag icon-md text-danger"></i>
                                                                      </div>
                                                                      <div style="color:ROYALBLUE;"><STRONG>Violation % till date</STRONG></div>
                                                                      <div class="dashed-bottom">20 <span class="percentage">%</span></div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                              <li class="list-inline-item">
                                                                  <aside>
                                                                      <div class="iconBlock">
                                                                          <i class="icon-truck icon-xl"></i>
                                                                      </div>
                                                                      <div class="text-bold">
                                                                          25<span class="percentage">%</span>
                                                                          <i class="icon-flag icon-md text-danger"></i>
                                                                      </div>
                                                                      <div style="color:ROYALBLUE;"><STRONG>Violation % till date</STRONG></div>
                                                                      <div class="dashed-bottom">20 <span class="percentage">%</span></div>
                                                                  </aside>
                                                                  <div class="date-time">18/07/02, 13:16</div>
                                                              </li>
                                                          </ul>
                                                      </div>
                                                  </div>
                                              </article>
                                              <article class="article dark-grey-bg border-bottom">
                                                  <div class="container">
                                                      <div class="content">
                                                          <h2 class="heading-text">Distribution of vehicle class by violation type</h2>
                                                          <p>Below table shows violation reason % for each class. Top 2 reasons for most of the vehicle types to violate traffic are "Restricted Vehicle" followed by defined "Speed limit" at that point. </p>
                                                      </div>
                                                      <div class="data-list row">
                                                          <div class="col label">
                                                              <div class="card">
                                                                  <figure class="card-img-top"></figure>
                                                                  <h5 class="card-title text-truncate">Motorcycle</h5>
                                                                  <ul class="list-group list-group-flush">
                                                                      <li class="list-group-item">
                                                                          <span>Vehicles movement in wrong direction</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>Overloaded Vehicle</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>Restricted Vehicles</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>No Entry</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>Speed Limit</span>
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                          <div class="col list">
                                                              <div class="card">
                                                                  <figure class="card-img-top">
                                                                      <span>
                                                                          <i class="icon-scooter icon-xl"></i>
                                                                      </span>
                                                                  </figure>
                                                                  <h5 class="card-title text-truncate">Motorcycle</h5>
                                                                  <ul class="list-group list-group-flush">
                                                                      <li class="list-group-item">
                                                                          <strong>9%</strong>
                                                                          <span class="d-block d-lg-none">Vehicles movement in wrong direction</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>18%</strong>
                                                                          <span class="d-block d-lg-none">Overloaded Vehicle</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>44%</strong>
                                                                          <span class="d-block d-lg-none">Restricted Vehicles</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>39%</strong>
                                                                          <span class="d-block d-lg-none">No Entry</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>32%</strong>
                                                                          <span class="d-block d-lg-none">Speed Limit</span>
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                          <div class="col list">
                                                              <div class="card">
                                                                  <figure class="card-img-top">
                                                                      <span>
                                                                          <i class="icon-auto-rickshaw icon-xl"></i>
                                                                      </span>
                                                                  </figure>
                                                                  <h5 class="card-title text-truncate">Three Wheeler</h5>
                                                                  <ul class="list-group list-group-flush">
                                                                      <li class="list-group-item">
                                                                          <strong>8%</strong>
                                                                          <span class="d-block d-lg-none">Vehicles movement in wrong direction</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>12%</strong>
                                                                          <span class="d-block d-lg-none">Overloaded Vehicle</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>54%</strong>
                                                                          <span class="d-block d-lg-none">Restricted Vehicles</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>32%</strong>
                                                                          <span class="d-block d-lg-none">No Entry</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>28%</strong>
                                                                          <span class="d-block d-lg-none">Speed Limit</span>
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                          <div class="col list">
                                                              <div class="card">
                                                                  <figure class="card-img-top">
                                                                      <span>
                                                                          <i class="icon-car icon-xl"></i>
                                                                      </span>
                                                                  </figure>
                                                                  <h5 class="card-title text-truncate">Car</h5>
                                                                  <ul class="list-group list-group-flush">
                                                                      <li class="list-group-item">
                                                                          <strong>8%</strong>
                                                                          <span class="d-block d-lg-none">Vehicles movement in wrong direction</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>12%</strong>
                                                                          <span class="d-block d-lg-none">Overloaded Vehicle</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>54%</strong>
                                                                          <span class="d-block d-lg-none">Restricted Vehicles</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>32%</strong>
                                                                          <span class="d-block d-lg-none">No Entry</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>28%</strong>
                                                                          <span class="d-block d-lg-none">Speed Limit</span>
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                          <div class="col list">
                                                              <div class="card">
                                                                  <figure class="card-img-top">
                                                                      <span>
                                                                          <i class="icon-bus icon-xl"></i>
                                                                      </span>
                                                                  </figure>
                                                                  <h5 class="card-title text-truncate">Bus</h5>
                                                                  <ul class="list-group list-group-flush">
                                                                      <li class="list-group-item">
                                                                          <strong>8%</strong>
                                                                          <span class="d-block d-lg-none">Vehicles movement in wrong direction</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>12%</strong>
                                                                          <span class="d-block d-lg-none">Overloaded Vehicle</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>54%</strong>
                                                                          <span class="d-block d-lg-none">Restricted Vehicles</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>32%</strong>
                                                                          <span class="d-block d-lg-none">No Entry</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>28%</strong>
                                                                          <span class="d-block d-lg-none">Speed Limit</span>
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                          <div class="col list">
                                                              <div class="card">
                                                                  <figure class="card-img-top">
                                                                      <span>
                                                                          <i class="icon-light-truck icon-xl"></i>
                                                                      </span>
                                                                  </figure>
                                                                  <h5 class="card-title text-truncate">Light Truck</h5>
                                                                  <ul class="list-group list-group-flush">
                                                                      <li class="list-group-item">
                                                                          <strong>8%</strong>
                                                                          <span class="d-block d-lg-none">Vehicles movement in wrong direction</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>12%</strong>
                                                                          <span class="d-block d-lg-none">Overloaded Vehicle</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>54%</strong>
                                                                          <span class="d-block d-lg-none">Restricted Vehicles</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>32%</strong>
                                                                          <span class="d-block d-lg-none">No Entry</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>28%</strong>
                                                                          <span class="d-block d-lg-none">Speed Limit</span>
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                          <div class="col list">
                                                              <div class="card">
                                                                  <figure class="card-img-top">
                                                                      <span>
                                                                          <i class="icon-truck icon-xl"></i>
                                                                      </span>
                                                                  </figure>
                                                                  <h5 class="card-title text-truncate">Truck</h5>
                                                                  <ul class="list-group list-group-flush">
                                                                      <li class="list-group-item">
                                                                          <strong>8%</strong>
                                                                          <span class="d-block d-lg-none">Vehicles movement in wrong direction</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>12%</strong>
                                                                          <span class="d-block d-lg-none">Overloaded Vehicle</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>54%</strong>
                                                                          <span class="d-block d-lg-none">Restricted Vehicles</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>32%</strong>
                                                                          <span class="d-block d-lg-none">No Entry</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>28%</strong>
                                                                          <span class="d-block d-lg-none">Speed Limit</span>
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </article>
                                              <article class="article dark-grey-bg border-bottom">
                                                  <div class="container">
                                                      <div class="content">
                                                          <h2 class="heading-text">Hourly Distribution of Speed Ranges</h2>
                                                          <p>Below table shows Hourly % for each class. 12 Speed Ranges fro 0-10 to 110-120. </p>
                                                      </div>
                                                      <div class="data-list row">
                                                          <div class="col label">
                                                              <div class="card">
                                                                  <figure class="card-img-top"></figure>
                                                                  <h5 class="card-title text-truncate">Motorcycle</h5>
                                                                  <ul class="list-group list-group-flush">
                                                                      <li class="list-group-item">
                                                                          <span>00:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>01:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>02:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>03:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>04:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>05:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>06:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>07:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>08:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>09:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>10:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>11:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>12:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>13:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>14:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>15:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>16:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>17:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>18:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>19:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>20:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>21:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>22:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>23:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <span>24:00</span>
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                          <div class="col list">
                                                              <div class="card">
                                                                  <figure class="card-img-top">
                                                                      <span>
                                                                          <i class="icon-scooter icon-xl"></i>
                                                                      </span>
                                                                  </figure>
                                                                  <h5 class="card-title text-truncate">Motorcycle</h5>
                                                                  <ul class="list-group list-group-flush">
                                                                   
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH0"></label></strong>
                                                                          <span class="d-block d-lg-none">00:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH1"></label></strong>
                                                                          <span class="d-block d-lg-none">01:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH2"></label></strong>
                                                                          <span class="d-block d-lg-none">02:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH3"></label></strong>
                                                                          <span class="d-block d-lg-none">03:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH4"></label></strong>
                                                                          <span class="d-block d-lg-none">04:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH5"></label></strong>
                                                                          <span class="d-block d-lg-none">05:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH6"></label></strong>
                                                                          <span class="d-block d-lg-none">06:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH7"></label></strong>
                                                                          <span class="d-block d-lg-none">07:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH8"></label></strong>
                                                                          <span class="d-block d-lg-none">08:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH9"></label></strong>
                                                                          <span class="d-block d-lg-none">09:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <div id="lbltotalMotorycleH10">Hi</div>
                                                                          <span class="d-block d-lg-none">10:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH11"></label></strong>
                                                                          <span class="d-block d-lg-none">11:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH12"></label></strong>
                                                                          <span class="d-block d-lg-none">12:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH13"></label></strong>
                                                                          <span class="d-block d-lg-none">13:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH14"></label></strong>
                                                                          <span class="d-block d-lg-none">14:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH15"></label></strong>
                                                                          <span class="d-block d-lg-none">15:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH16"></label></strong>
                                                                          <span class="d-block d-lg-none">16:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH17"></label></strong>
                                                                          <span class="d-block d-lg-none">17:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH18"></label></strong>
                                                                          <span class="d-block d-lg-none">18:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH19"></label></strong>
                                                                          <span class="d-block d-lg-none">19:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH20"></label></strong>
                                                                          <span class="d-block d-lg-none">20:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH21"></label></strong>
                                                                          <span class="d-block d-lg-none">21:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH22"></label></strong>
                                                                          <span class="d-block d-lg-none">22:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH23"></label></strong>
                                                                          <span class="d-block d-lg-none">23:00</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong><label id="lbltotalMotorycleH24"></label></strong>
                                                                          <span class="d-block d-lg-none">24:00</span>
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                          <div class="col list">
                                                              <div class="card">
                                                                  <figure class="card-img-top">
                                                                      <span>
                                                                          <i class="icon-auto-rickshaw icon-xl"></i>
                                                                      </span>
                                                                  </figure>
                                                                  <h5 class="card-title text-truncate">Three Wheeler</h5>
                                                                  <ul class="list-group list-group-flush">
                                                                      <li class="list-group-item">
                                                                          <strong>8%</strong>
                                                                          <span class="d-block d-lg-none">Vehicles movement in wrong direction</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>12%</strong>
                                                                          <span class="d-block d-lg-none">Overloaded Vehicle</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>54%</strong>
                                                                          <span class="d-block d-lg-none">Restricted Vehicles</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>32%</strong>
                                                                          <span class="d-block d-lg-none">No Entry</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>28%</strong>
                                                                          <span class="d-block d-lg-none">Speed Limit</span>
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                          <div class="col list">
                                                              <div class="card">
                                                                  <figure class="card-img-top">
                                                                      <span>
                                                                          <i class="icon-car icon-xl"></i>
                                                                      </span>
                                                                  </figure>
                                                                  <h5 class="card-title text-truncate">Car</h5>
                                                                  <ul class="list-group list-group-flush">
                                                                      <li class="list-group-item">
                                                                          <strong>8%</strong>
                                                                          <span class="d-block d-lg-none">Vehicles movement in wrong direction</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>12%</strong>
                                                                          <span class="d-block d-lg-none">Overloaded Vehicle</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>54%</strong>
                                                                          <span class="d-block d-lg-none">Restricted Vehicles</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>32%</strong>
                                                                          <span class="d-block d-lg-none">No Entry</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>28%</strong>
                                                                          <span class="d-block d-lg-none">Speed Limit</span>
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                          <div class="col list">
                                                              <div class="card">
                                                                  <figure class="card-img-top">
                                                                      <span>
                                                                          <i class="icon-bus icon-xl"></i>
                                                                      </span>
                                                                  </figure>
                                                                  <h5 class="card-title text-truncate">Bus</h5>
                                                                  <ul class="list-group list-group-flush">
                                                                      <li class="list-group-item">
                                                                          <strong>8%</strong>
                                                                          <span class="d-block d-lg-none">Vehicles movement in wrong direction</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>12%</strong>
                                                                          <span class="d-block d-lg-none">Overloaded Vehicle</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>54%</strong>
                                                                          <span class="d-block d-lg-none">Restricted Vehicles</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>32%</strong>
                                                                          <span class="d-block d-lg-none">No Entry</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>28%</strong>
                                                                          <span class="d-block d-lg-none">Speed Limit</span>
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                          <div class="col list">
                                                              <div class="card">
                                                                  <figure class="card-img-top">
                                                                      <span>
                                                                          <i class="icon-light-truck icon-xl"></i>
                                                                      </span>
                                                                  </figure>
                                                                  <h5 class="card-title text-truncate">Light Truck</h5>
                                                                  <ul class="list-group list-group-flush">
                                                                      <li class="list-group-item">
                                                                          <strong>8%</strong>
                                                                          <span class="d-block d-lg-none">Vehicles movement in wrong direction</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>12%</strong>
                                                                          <span class="d-block d-lg-none">Overloaded Vehicle</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>54%</strong>
                                                                          <span class="d-block d-lg-none">Restricted Vehicles</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>32%</strong>
                                                                          <span class="d-block d-lg-none">No Entry</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>28%</strong>
                                                                          <span class="d-block d-lg-none">Speed Limit</span>
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                          <div class="col list">
                                                              <div class="card">
                                                                  <figure class="card-img-top">
                                                                      <span>
                                                                          <i class="icon-truck icon-xl"></i>
                                                                      </span>
                                                                  </figure>
                                                                  <h5 class="card-title text-truncate">Truck</h5>
                                                                  <ul class="list-group list-group-flush">
                                                                      <li class="list-group-item">
                                                                          <strong>8%</strong>
                                                                          <span class="d-block d-lg-none">Vehicles movement in wrong direction</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>12%</strong>
                                                                          <span class="d-block d-lg-none">Overloaded Vehicle</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>54%</strong>
                                                                          <span class="d-block d-lg-none">Restricted Vehicles</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>32%</strong>
                                                                          <span class="d-block d-lg-none">No Entry</span>
                                                                      </li>
                                                                      <li class="list-group-item">
                                                                          <strong>28%</strong>
                                                                          <span class="d-block d-lg-none">Speed Limit</span>
                                                                      </li>
                                                                  </ul>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </article>  
                                              <article class="article">
                                                  <div class="container">
                                                      <div class="content">
                                                          <h2 class="heading-text">Traffic Distribution Hub</h2>
                                                          <p>Compare and track traffic distribution on parameters like Congestion, Speed, Probable Violation and Risk factor </p>
                                                          <div>
                                                              <div class="form-check form-check-inline">
                                                                  <input class="form-check-input" type="checkbox" id="all" checked>
                                                                  <label class="form-check-label" for="all">All</label>
                                                              </div>
                                                              <div class="form-check form-check-inline">
                                                                  <input class="form-check-input" type="checkbox" id="cars">
                                                                  <label class="form-check-label" for="cars">Cars</label>
                                                              </div>
                                                              <div class="form-check form-check-inline">
                                                                  <input class="form-check-input" type="checkbox" id="Buses">
                                                                  <label class="form-check-label" for="Buses">Buses</label>
                                                              </div>
                                                              <div class="form-check form-check-inline">
                                                                  <input class="form-check-input" type="checkbox" id="Trucks">
                                                                  <label class="form-check-label" for="Trucks">Trucks</label>
                                                              </div>
                                                              <div class="form-check form-check-inline">
                                                                  <input class="form-check-input" type="checkbox" id="Light-Trucks">
                                                                  <label class="form-check-label" for="Light-Trucks">Light Trucks</label>
                                                              </div>
                                                              <div class="form-check form-check-inline">
                                                                  <input class="form-check-input" type="checkbox" id="Whellers-2">
                                                                  <label class="form-check-label" for="Whellers-2">2 Whellers</label>
                                                              </div>
                                                              <div class="form-check form-check-inline">
                                                                  <input class="form-check-input" type="checkbox" id="Whellers-3">
                                                                  <label class="form-check-label" for="Whellers-3">3 Whellers</label>
                                                              </div>
                                                          </div>
                                                      </div>
                                                      <div class="graph-panel">
                                                          <div class="row">
                                                              <div class="col-md-6 panel">
                                                                  <div class="heading pb-2">
                                                                      <h6>All</h6>
                                                                      <h5>Speed wise distribution</h5>
                                                                  </div>
                                                                  <div class="graph">
                                                                      <img src="images/graph.png" class="img-fluid" />
                                                                  </div>
                                                                  <div class="article pt-4 pb-0">
                                                                      <div class="content text-left">
                                                                          <h4 class="heading-text text-size-sm mb-1">Speed wise distribution</h4>
                                                                          <p>Some commentary...</p>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                              <div class="col-md-6 panel">
                                                                  <div class="heading pb-2">
                                                                      <h6>All</h6>
                                                                      <h5>Traffic congestion by time</h5>
                                                                  </div>
                                                                  <div class="graph">
                                                                      <img src="images/graph.png" class="img-fluid" />
                                                                  </div>
                                                                  <div class="article pt-4 pb-0">
                                                                      <div class="content text-left">
                                                                          <h4 class="heading-text text-size-sm mb-1">Traffic congestion by time</h4>
                                                                          <p>Some commentary...</p>
                                                                      </div>
                                                                  </div>
                                                              </div>

                                                              <div class="col-md-6 panel">
                                                                  <div class="heading pb-2">
                                                                      <h6>All</h6>
                                                                      <h5>Traffic Violation distribution</h5>
                                                                  </div>
                                                                  <div class="graph">
                                                                      <img src="images/graph.png" class="img-fluid" />
                                                                  </div>
                                                                  <div class="article pt-4 pb-0">
                                                                      <div class="content text-left">
                                                                          <h4 class="heading-text text-size-sm mb-1">Speed wise distribution</h4>
                                                                          <p>Some commentary...</p>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                              <div class="col-md-6 panel">
                                                                  <div class="heading pb-2">
                                                                      <h6>All</h6>
                                                                      <h5>Risk Factor distribution</h5>
                                                                  </div>
                                                                  <div class="graph">
                                                                      <img src="images/graph.png" class="img-fluid" />
                                                                  </div>
                                                                  <div class="article pt-4 pb-0">
                                                                      <div class="content text-left">
                                                                          <h4 class="heading-text text-size-sm mb-1">Traffic congestion by time</h4>
                                                                          <p>Some commentary...</p>
                                                                      </div>
                                                                  </div>
                                                              </div>

                                                              <div class="col-md-6 panel">
                                                                  <div class="heading pb-2">
                                                                      <h6>All</h6>
                                                                      <h5>Peak Traffic distribution</h5>
                                                                  </div>
                                                                  <div class="graph" style="height:250px; width:450px; background:#000000">
                                                                      <img src="images/graph.png" class="img-fluid" />
                                                                     
                                                                  </div>
                                                                  <div class="article pt-4 pb-0">
                                                                      <div class="content text-left">
                                                                          <h4 class="heading-text text-size-sm mb-1">Traffic congestion by time</h4>
                                                                          <p>Some commentary...</p>
                                                                      </div>
                                                                  </div>
                                                              </div>

                                                              <div class="col-md-6 panel">
                                                                  <div class="heading pb-2">
                                                                      <h6>All</h6>
                                                                      <h5>Vehicle Classification Distribution</h5>
                                                                  </div>
                                                                  <div class="graph" style="height:250px; width:450px; background:#000000">
                                                                      <img src="images/graph.png" class="img-fluid" />
                                                                     
                                                                  </div>
                                                                  <div class="article pt-4 pb-0">
                                                                      <div class="content text-left">
                                                                          <h4 class="heading-text text-size-sm mb-1">Traffic congestion by time</h4>
                                                                          <p>Some commentary...</p>
                                                                      </div>
                                                                  </div>
                                                              </div>

                                                          </div>
                                                      </div>
                                                  </div>
                                              </article>
                                          </div>
                                      </div></div>
                                          <!-- /#page-wrapper -->
<!-- footers -->
<?php require_once $abs_us_root.$us_url_root.'users/includes/page_footer.php'; // the final html footer copyright row + the external js calls ?>

<!-- Place any per-page javascript here -->
<?php require_once $abs_us_root.$us_url_root.'users/includes/html_footer.php'; // currently just the closing /body and /html ?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>          
<script type="text/javascript"> 
    
   
    TotalVehicleCount=0;
    var TotalMotorCycleCount=0;
    var TotalThreeWheelerCount=0;
    var TotalCarCount=0;
    var TotalBusCount=0;
    var TotalLightTruckCount=0;
    var TotalTruckCount=0;
    
    var jsonData=$.ajax({
            type: "GET",
            url: 'http://54.179.184.250/TrafficManagementAPI',
            data :  {Controller: 'VehicleAPIController',Function: 'TrafficListing',Table: 'sp_VehicleCurrentCount',Dtype1: 'Class',Dtype2: 'None',Dtype3: 'Count'},
            dataType: 'json',
            async: false
}).responseText;


                $.each(JSON.parse(jsonData), function(idx, obj) {
                     TotalVehicleCount=TotalVehicleCount + obj.Count;
                     if(obj.Class==='Class1')
                    {
                         TotalMotorCycleCount=TotalMotorCycleCount + obj.Count;
                     }
                     if(obj.Class==='Class2')
                     {
                         TotalThreeWheelerCount=TotalThreeWheelerCount + obj.Count;
                     }
                     if(obj.Class==='Class3')
                     {
                         TotalCarCount=TotalCarCount + obj.Count;
                     }
                     if(obj.Class==='Class4')
                     {
                         TotalBusCount=TotalBusCount + obj.Count;
                     }
                     if(obj.Class==='Class5')
                     {
                         TotalLightTruckCount=TotalLightTruckCount + obj.Count;
                     }
                     if(obj.Class==='Class6')
                     {
                         TotalTruckCount=TotalTruckCount + obj.Count;
                     }
                });
                
                document.getElementById('lbltotalMotorycle').innerHTML=TotalVehicleCount;
                document.getElementById('lblMotoryclePercentage').innerHTML=((TotalMotorCycleCount/TotalVehicleCount)*100).toFixed(1);
                document.getElementById('lblMotoryclePercentageDesc').innerHTML=((TotalMotorCycleCount/TotalVehicleCount)*100).toFixed(1);
                
                document.getElementById('lbltotalThreeWheeler').innerHTML=TotalThreeWheelerCount;
                document.getElementById('lblThreeWheelerPercentage').innerHTML=((TotalThreeWheelerCount/TotalVehicleCount)*100).toFixed(1);
                
                 document.getElementById('lbltotalCar').innerHTML=TotalCarCount;
                document.getElementById('lblCarPercentage').innerHTML=((TotalCarCount/TotalVehicleCount)*100).toFixed(1);
                document.getElementById('lblCarPercentageDesc').innerHTML=((TotalCarCount/TotalVehicleCount)*100).toFixed(1);
                
                 document.getElementById('lbltotalBus').innerHTML=TotalBusCount;
                document.getElementById('lblBusPercentage').innerHTML=((TotalBusCount/TotalVehicleCount)*100).toFixed(1);
                
                 document.getElementById('lbltotalLightTruck').innerHTML=TotalLightTruckCount;
                document.getElementById('lblLightTruckPercentage').innerHTML=((TotalLightTruckCount/TotalVehicleCount)*100).toFixed(1);
                
                 document.getElementById('lbltotalTruck').innerHTML=TotalTruckCount;
                document.getElementById('lblTruckPercentage').innerHTML=((TotalTruckCount/TotalVehicleCount)*100).toFixed(1);
                document.getElementById('lblTruckPercentageDesc').innerHTML=((TotalTruckCount/TotalVehicleCount)*100).toFixed(1);
               
    google.load('visualization', '1.2', {'packages':['corechart']});
    google.setOnLoadCallback(drawChart);
    //google.charts.load('current', {packages: ["line"]});
    google.charts.load('current', {packages: ['controls', 'corechart'],callback: drawDashboard});
    google.charts.setOnLoadCallback(drawDashboard);
    function drawChart() {
                var jsonDataSpeed=$.ajax({
                            type: "GET",
                            url: 'http://54.179.184.250/TrafficManagementAPI',
                            data :  {Controller: 'VehicleAPIController',Function: 'VehicleHourlySpeedRangeData'},
                            dataType: 'json',
                            async: false
                }).responseText;
                
                var TotalClass1CountHour10_0_10=0;
                var TotalClass1CountHour10_10_20=0;
                var TotalClass1CountHour10_20_30=0;
                var TotalClass1CountHour10_30_40=0;
                var TotalClass1CountHour10_40_50=0;
                var TotalClass1CountHour10_50_60=0;
                var TotalClass1CountHour10_60_70=0;
                var TotalClass1CountHour10_70_80=0;
                var TotalClass1CountHour10_80_90=0;
                var TotalClass1CountHour10_90_100=0;
                var TotalClass1CountHour10_100_110=0;
                var TotalClass1CountHour10_110_120=0;
                
                var TotalClass1CountHour11_0_10=0;
                var TotalClass1CountHour11_10_20=0;
                var TotalClass1CountHour11_20_30=0;
                var TotalClass1CountHour11_30_40=0;
                var TotalClass1CountHour11_40_50=0;
                var TotalClass1CountHour11_50_60=0;
                var TotalClass1CountHour11_60_70=0;
                var TotalClass1CountHour11_70_80=0;
                var TotalClass1CountHour11_80_90=0;
                var TotalClass1CountHour11_90_100=0;
                var TotalClass1CountHour11_100_110=0;
                var TotalClass1CountHour11_110_120=0;
                
                var TotalClass1CountHour12_0_10=0;
                var TotalClass1CountHour12_10_20=0;
                var TotalClass1CountHour12_20_30=0;
                var TotalClass1CountHour12_30_40=0;
                var TotalClass1CountHour12_40_50=0;
                var TotalClass1CountHour12_50_60=0;
                var TotalClass1CountHour12_60_70=0;
                var TotalClass1CountHour12_70_80=0;
                var TotalClass1CountHour12_80_90=0;
                var TotalClass1CountHour12_90_100=0;
                var TotalClass1CountHour12_100_110=0;
                var TotalClass1CountHour12_110_120=0;
                
                var TotalClass1CountHour13_0_10=0;
                var TotalClass1CountHour13_10_20=0;
                var TotalClass1CountHour13_20_30=0;
                var TotalClass1CountHour13_30_40=0;
                var TotalClass1CountHour13_40_50=0;
                var TotalClass1CountHour13_50_60=0;
                var TotalClass1CountHour13_60_70=0;
                var TotalClass1CountHour13_70_80=0;
                var TotalClass1CountHour13_80_90=0;
                var TotalClass1CountHour13_90_100=0;
                var TotalClass1CountHour13_100_110=0;
                var TotalClass1CountHour13_110_120=0;
                $.each(JSON.parse(jsonDataSpeed), function(idx, obj) {
                     
                     if(obj.Class=='Class1')
                    {
                        if(obj.Hour=='0')
                        {
                         TotalClass1CountHour0_0_10=obj.S10;
                         TotalClass1CountHour0_10_20=obj.S20;
                         TotalClass1CountHour0_20_30=obj.S30;
                         TotalClass1CountHour0_3_40=obj.S40;
                         TotalClass1CountHour0_40_50=obj.S50;
                         TotalClass1CountHour0_50_60=obj.S60;
                         TotalClass1CountHour0_60_70=obj.S70;
                         TotalClass1CountHour0_70_80=obj.S80;
                         TotalClass1CountHour0_80_90=obj.S90;
                         TotalClass1CountHour0_90_100=obj.S100;
                         TotalClass1CountHour0_100_110=obj.S110;
                         TotalClass1CountHour0_110_120=obj.S120;
                        }
                        if(obj.Hour=='10')
                        {
                         TotalClass1CountHour10_0_10=obj.S10;
                         TotalClass1CountHour10_10_20=obj.S20;
                         TotalClass1CountHour10_20_30=obj.S30;
                         TotalClass1CountHour10_30_40=obj.S40;
                         TotalClass1CountHour10_40_50=obj.S50;
                         TotalClass1CountHour10_50_60=obj.S60;
                         TotalClass1CountHour10_60_70=obj.S70;
                         TotalClass1CountHour10_70_80=obj.S80;
                         TotalClass1CountHour10_80_90=obj.S90;
                         TotalClass1CountHour10_90_100=obj.S100;
                         TotalClass1CountHour10_100_110=obj.S110;
                         TotalClass1CountHour10_110_120=obj.S120;
                        }
                        if(obj.Hour=='11')
                        {
                         TotalClass1CountHour11_0_10=obj.S10;
                         TotalClass1CountHour11_10_20=obj.S20;
                         TotalClass1CountHour11_20_30=obj.S30;
                         TotalClass1CountHour11_30_40=obj.S40;
                         TotalClass1CountHour11_40_50=obj.S50;
                         TotalClass1CountHour11_50_60=obj.S60;
                         TotalClass1CountHour11_60_70=obj.S70;
                         TotalClass1CountHour11_70_80=obj.S80;
                         TotalClass1CountHour11_80_90=obj.S90;
                         TotalClass1CountHour11_90_100=obj.S100;
                         TotalClass1CountHour11_100_110=obj.S110;
                         TotalClass1CountHour11_110_120=obj.S120;
                        }
                        if(obj.Hour==='12')
                        {
                         TotalClass1CountHour12_0_10=obj.S10;
                         TotalClass1CountHour12_10_20=obj.S20;
                         TotalClass1CountHour12_20_30=obj.S30;
                         TotalClass1CountHour12_30_40=obj.S40;
                         TotalClass1CountHour12_40_50=obj.S50;
                         TotalClass1CountHour12_50_60=obj.S60;
                         TotalClass1CountHour12_60_70=obj.S70;
                         TotalClass1CountHour12_70_80=obj.S80;
                         TotalClass1CountHour12_80_90=obj.S90;
                         TotalClass1CountHour12_90_100=obj.S100;
                         TotalClass1CountHour12_100_110=obj.S110;
                         TotalClass1CountHour12_110_120=obj.S120;
                        }
                        if(obj.Hour==='13')
                        {
                         TotalClass1CountHour13_0_10=obj.S10;
                         TotalClass1CountHour13_10_20=obj.S20;
                         TotalClass1CountHour13_20_30=obj.S30;
                         TotalClass1CountHour13_30_40=obj.S40;
                         TotalClass1CountHour13_40_50=obj.S50;
                         TotalClass1CountHour13_50_60=obj.S60;
                         TotalClass1CountHour13_60_70=obj.S70;
                         TotalClass1CountHour13_70_80=obj.S80;
                         TotalClass1CountHour13_80_90=obj.S90;
                         TotalClass1CountHour13_90_100=obj.S100;
                         TotalClass1CountHour13_100_110=obj.S110;
                         TotalClass1CountHour13_110_120=obj.S120;
                        }
                     }
                });
               
                //var chart = new google.visualization.PieChart(document.getElementById('lbltotalMotorycleH10'));
               // alert(document.getElementById('lbltotalMotorycleH10'));
               
               var data = google.visualization.arrayToDataTable([
                ['Speed Range', 'Count'],
                ['0-10',     TotalClass1CountHour10_0_10],
                ['10-20',      TotalClass1CountHour10_10_20],
                ['20-30',  TotalClass1CountHour10_20_30],
                ['30-40', TotalClass1CountHour10_30_40],
                ['40-50',    TotalClass1CountHour10_40_50],
                ['50-60',    TotalClass1CountHour10_50_60],
                ['60-70',    TotalClass1CountHour10_60_70],
                ['70-80',    TotalClass1CountHour10_70_80],
                ['80-90',    TotalClass1CountHour10_80_90],
                ['90-100',    TotalClass1CountHour10_90_100],
                ['100-110',    TotalClass1CountHour10_100_110],
                ['110-120',    TotalClass1CountHour10_110_120]
              ]);
              var options = {backgroundColor: '#676767',
                        width: 180,
                        height: 50,
                        pointSize: 4,
                        legend: {position: 'left'},
                        chartArea: {
                            left: 0,
                            top: 0,
                            width: 180,
                            height: 50},
                        vAxis: {
                            baselineColor: '#fff',
                            gridlines: {color: 'transparent'}
                        },
                        
                        annotation: {
                            1: {
                                style: 'none'
                            }
                        }
                    };
              var chart = new google.visualization.PieChart(document.getElementById('lbltotalMotorycleH10'));
              chart.draw(data,options);

              var data = google.visualization.arrayToDataTable([
                ['Speed Range', 'Count'],
                ['0-10',     TotalClass1CountHour11_0_10],
                ['10-20',      TotalClass1CountHour11_10_20],
                ['20-30',  TotalClass1CountHour11_20_30],
                ['30-40', TotalClass1CountHour11_30_40],
                ['40-50',    TotalClass1CountHour11_40_50],
                ['50-60',    TotalClass1CountHour11_50_60],
                ['60-70',    TotalClass1CountHour11_60_70],
                ['70-80',    TotalClass1CountHour11_70_80],
                ['80-90',    TotalClass1CountHour11_80_90],
                ['90-100',    TotalClass1CountHour11_90_100],
                ['100-110',    TotalClass1CountHour11_100_110],
                ['110-120',    TotalClass1CountHour11_110_120]
              ]);
              
              var chart = new google.visualization.PieChart(document.getElementById('lbltotalMotorycleH11'));
              chart.draw(data,options);
                
              var data = google.visualization.arrayToDataTable([
                ['Speed Range', 'Count'],
                ['0-10',     TotalClass1CountHour12_0_10],
                ['10-20',      TotalClass1CountHour12_10_20],
                ['20-30',  TotalClass1CountHour12_20_30],
                ['30-40', TotalClass1CountHour12_30_40],
                ['40-50',    TotalClass1CountHour12_40_50],
                ['50-60',    TotalClass1CountHour12_50_60],
                ['60-70',    TotalClass1CountHour12_60_70],
                ['70-80',    TotalClass1CountHour12_70_80],
                ['80-90',    TotalClass1CountHour12_80_90],
                ['90-100',    TotalClass1CountHour12_90_100],
                ['100-110',    TotalClass1CountHour12_100_110],
                ['110-120',    TotalClass1CountHour12_110_120]
              ]);
              
              var chart = new google.visualization.PieChart(document.getElementById('lbltotalMotorycleH12'));
              chart.draw(data, options);  
            
                var data = google.visualization.arrayToDataTable([
                ['Speed Range', 'Count'],
                ['0-10',     TotalClass1CountHour13_0_10],
                ['10-20',      TotalClass1CountHour13_10_20],
                ['20-30',  TotalClass1CountHour13_20_30],
                ['30-40', TotalClass1CountHour13_30_40],
                ['40-50',    TotalClass1CountHour13_40_50],
                ['50-60',    TotalClass1CountHour13_50_60],
                ['60-70',    TotalClass1CountHour13_60_70],
                ['70-80',    TotalClass1CountHour13_70_80],
                ['80-90',    TotalClass1CountHour13_80_90],
                ['90-100',    TotalClass1CountHour13_90_100],
                ['100-110',    TotalClass1CountHour13_100_110],
                ['110-120',    TotalClass1CountHour13_110_120]
              ]);
              
              var chart = new google.visualization.PieChart(document.getElementById('lbltotalMotorycleH13'));
              chart.draw(data, options);
            }
            
</script>