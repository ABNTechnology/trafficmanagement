<?php
/*
UserSpice 4
An Open Source PHP User Management System
by the UserSpice Team at http://UserSpice.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
?>
<?php require_once 'init.php'; ?>
<?php require_once $abs_us_root.$us_url_root.'users/includes/header.php'; ?>
<?php require_once $abs_us_root.$us_url_root.'users/includes/navigation.php'; ?>
<?php if (!securePage($_SERVER['PHP_SELF'])){die();}?>
    <style>
        #map {
            height: 100%;
            width: 100%;
        }
    </style>
            <div class="content-area">
                <div id="map"></div>
            </div>
        
   <?php
    
    /* lat/lng data will be added to this array */
    $dropdown_Sensor_result = $db->query('SELECT * FROM sensors');
    $dropdowns_Sensor = $dropdown_Sensor_result->results();
    $locations=array();
 
        foreach ($dropdowns_Sensor as $dropdown_Sensor) {
            $nama_kabkot = $dropdown_Sensor->sensorlandmark;
            $longitude =   $dropdown_Sensor->longitude;
            $latitude =    $dropdown_Sensor->latitude;
            /* Each row is added as a new array */
            $locations[]=array( 'name'=>$nama_kabkot, 'lat'=>$latitude, 'lng'=>$longitude );
        }
        /* Convert data to json */
        $markers = json_encode( $locations );
?>   
  
    
<script>
    <?php
    
    
        echo "var markers=$markers;\n";

    ?>

    function initMap() {
        var latlng = new google.maps.LatLng(20.5937, 79.044);
        var myOptions = {
            zoom: 5,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false
        };

        var map = new google.maps.Map(document.getElementById("map"),myOptions);
        var infowindow = new google.maps.InfoWindow(), marker, lat, lng;
        var json=markers;

        for( var o in json ){
            lat = json[o].lat;
            lng=json[o].lng;
            name=json[o].name;

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat,lng),
                name:name,
                map: map
                
            }); 
            google.maps.event.addListener( marker, 'click', function(e){
                infowindow.setContent( this.name );
                infowindow.open( map, this );
            }.bind( marker ) );
        }
    }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBacEyl-zVeoKr1m1z-rSFomx7JKJy_h2k&callback=initMap">
    </script>

<!-- footers -->
<?php require_once $abs_us_root.$us_url_root.'users/includes/page_footer.php'; // the final html footer copyright row + the external js calls ?>

<!-- Place any per-page javascript here -->

<?php require_once $abs_us_root.$us_url_root.'users/includes/html_footer.php'; // currently just the closing /body and /html 