<?php
/*
UserSpice 4
An Open Source PHP User Management System
by the UserSpice Team at http://UserSpice.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
?>



<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
// (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
// function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
// e=o.createElement(i);r=o.getElementsByTagName(i)[0];
// e.src='//www.google-analytics.com/analytics.js';
// r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
// ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>
<div class="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				&copy; <?php echo date("Y"); ?> <?=$settings->copyright; ?>
			</div>
			<div class="col-sm-6 text-center text-md-right">
				<ul class="list-inline m-0 p-0">
                    <li class="list-inline-item">
                        <a href="#">Terms and Conditions</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#">Privacy Policy</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#"><i class="icon-fb icon-sm"></i></a>
                    </li>
                    <li class="list-inline-item">
                        <a href="#"><i class="icon-twitter icon-sm"></i></a>
                    </li>
                </ul>
			</div>
		</div>
	</div>
    
</div>
                        